/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>


// this is the directory used by the voxl-hello-server for named pipes
#define PIPE_NAME	"hello"

// you may need a larger buffer for your application!
#define PIPE_READ_BUF_SIZE	1024
#define CLIENT_NAME	"modal-hello-pause"

static int en_debug;


static void _print_usage(void)
{
	printf("\n\
This is a test of libmodal_pipe. It connects to the pipe dir /run/mpa/hello/\n\
created by modal-hello-server and toggles between a paused and running state.\n\
\n\
Run this in debug mode to enable debug prints in the libmodal_pipe client code.\n\
You can also try the auto-reconnect mode which is useful for some projects but\n\
not all. This is a good example of using the simple helper feature.\n\
\n\
See the voxl-inspect-* examples in the voxl-mpa-tools repository for examples on\n\
reading other sorts of MPA data such as camera and IMU data\n\
\n\
-d, --debug                 print debug info\n\
-h, --help                  print this help message\n\
\n");
	return;
}

// called whenever the simple helper has data for us to process
static void _simple_cb(int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	printf("received %d bytes on channel %d: %s\n", bytes, ch, data);
	return;
}

// called whenever we connect or reconnect to the server
static void _connect_cb(int ch, __attribute__((unused)) void* context)
{
	int ret;
	fprintf(stderr, "channel %d connected to server\n", ch);

	// send a hello message back to server via the control pipe (for fun)
	// not all servers will have a control pie, it's optional
	printf("sending hello to server control pipe\n");
	ret = pipe_client_send_control_cmd(0, "hello from client!");
	if(ret<0){
		fprintf(stderr, "failed to send control command to server\n");
		pipe_print_error(ret);
	}

	// now we are connected and before we read data,
	// check that the type is correct!!!
	if(!pipe_is_type(PIPE_NAME, "text")){
		fprintf(stderr, "ERROR, pipe is not of type \"text\"\n");
		main_running = 0;
		return;
	}

	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "channel %d disconnected from server\n", ch);
	return;
}


// not many command line arguments
static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",			no_argument,		0,	'd'},
		{"help",			no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};
	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "dh", long_options, &option_index);
		if(c == -1) break; // Detect the end of the options.
		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'd':
			en_debug = 1;
			break;
		case 'h':
			_print_usage();
			return -1;
		default:
			_print_usage();
			return -1;
		}
	}
	return 0;
}


int main(int argc, char* argv[])
{
	int ret;

	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// for this test we will use the simple helper with optional debug mode
	int flags						 = CLIENT_FLAG_EN_SIMPLE_HELPER;
	flags							|= CLIENT_FLAG_START_PAUSED;
	if(en_debug)			flags   |= CLIENT_FLAG_EN_DEBUG_PRINTS;

	// in auto-reconnect mode, tell the user we are waiting
	printf("waiting for server\n");

	// assign callabcks for data, connection, and disconnect. the "NULL" arg
	// here can be an optional void* context pointer passed back to the callbacks
	pipe_client_set_simple_helper_cb(0, _simple_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// init connection to server. In auto-reconnect mode this will "succeed"
	// even if the server is offline, but it will connect later on automatically
	ret = pipe_client_open(0, PIPE_NAME, CLIENT_NAME, flags, PIPE_READ_BUF_SIZE);

	// check for success
	if(ret){
		fprintf(stderr, "ERROR opening channel:\n");
		pipe_print_error(ret);
		if(ret==PIPE_ERROR_SERVER_NOT_AVAILABLE){
			fprintf(stderr, "make sure to start modal-hello-server first!\n");
		}
		return -1;
	}

	// we started paused with the CLIENT_FLAG_START_PAUSED
	int is_paused = 1;

	// keep going until signal handler sets the main_running flag to 0
	while(main_running){
		usleep(1000000);
		if(is_paused){
			printf("\nresuming\n");
			pipe_client_resume(0);
			is_paused = 0;
		}
		else{
			printf("pausing\n");
			pipe_client_pause(0);
			printf("paused\n");
			is_paused = 1;
		}
	}

	// all done, signal pipe read threads to stop
	printf("closing\n");
	fflush(stdout);
	pipe_client_close_all();

	return 0;
}