/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define _GNU_SOURCE		// for pthread_timedjoin_np and possibly other things
#include <stdio.h>		// for fprintf
#include <stdlib.h>		// for fprintf
#include <unistd.h>		// for read() & write()
#include <errno.h>		// to check error in read() and write()
#include <sys/ioctl.h>
#include <fcntl.h>		// for O_WRONLY & O_RDONLY
#include <string.h>		// for strlen()
#include <sys/stat.h>	// for mkfifo()
#include <sys/types.h>	// for mkfifo()
#include <signal.h>		// for pthread_kill
#include <pthread.h>
#include <ftw.h>		// for file tree walk
#include <modal_json.h>

#include <modal_pipe_server.h>
#include "misc.h"
#ifdef __ANDROID__
#include <android/log.h>

#define LOG_TAG "LIBMODAL_PIPE"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define ASSERT(cond, fmt, ...)                                \
  if (!(cond)) {                                              \
    __android_log_assert(#cond, LOG_TAG, fmt, ##__VA_ARGS__); \
  }

#endif // __ANDROID__

// shorten these names to improve code readability
#define N_CH		PIPE_SERVER_MAX_CHANNELS
#define N_CLIENT	PIPE_SERVER_MAX_CLIENTS_PER_CH
#define DIR_LEN		MODAL_PIPE_MAX_DIR_LEN
#define NAME_LEN	MODAL_PIPE_MAX_NAME_LEN
#define PATH_LEN	MODAL_PIPE_MAX_PATH_LEN


// struct containing the full state of each channel
typedef struct server_channel_t{

	// this first section are things that are manipulated by internal functions
	// in normal operation. Most are protected by a mutex. If you add anything
	// here in the future, make sure it's cleanup up by _wipe_channel()!!
	int			running;					///< set to 1 once a channel is running
	char		base_dir[DIR_LEN];			///< string containing base directory for each channel
	char		request_path[DIR_LEN+16];	///< string containing request pipe path for each channel
	char		control_path[DIR_LEN+16];	///< string containing control pipe path for each channel
	char		info_path[DIR_LEN+16];		///< string containing info pipe path for each channel
	int			request_fd;					///< one request pipe per channel, store fd here
	int			control_fd;					///< one control pipe per channel, store fd here
	pthread_t	request_thread;				///< thread ID reading the request pipe
	pthread_t	control_thread;				///< thread ID reading the control pipe
	int			n_clients;					///< number of clients in any state
	int			data_fd[N_CLIENT];			///< 2D array of all file descriptors for data pipes
	char		data_path[N_CLIENT][PATH_LEN];		///< store path of each data pipe to check for duplicates
	char		client_names[N_CLIENT][NAME_LEN];	///< store name of each client
	int			client_state[N_CLIENT];		///< keep track as clients connect and disconnect
	pipe_info_t	info;
	cJSON*		info_json;

	// everything below is set by the user as a configurable option before
	// creating the pipe. None of these things are wiped in _wipe_channel()
	int						control_pipe_size;		// global variable, so starts at 0
	int						control_read_buf_size;	// global variable, so starts at 0
	server_control_cb*		control_cb_func;
	server_request_cb*		request_cb_func;		// DEPRECATED
	server_connect_cb*		connect_cb_func;
	server_disconnect_cb*	disconnect_cb_func;
	void*					control_cb_context;
	void*					connect_cb_context;
	void*					request_cb_context;		// DEPRECATED
	void*					disconnect_cb_context;
} server_channel_t;


// array of structs defining the state of each channel
static server_channel_t c[N_CH];
// set to 1 to enable debug prints
static int en_debug;
// each channel gets a mutex to protect from writing during init or cleanup
static pthread_mutex_t mtx[N_CH];



// dummy function to catch the USR1 signal
static void _sigusr_cb(__attribute__((unused)) int sig)
{
	if(en_debug) printf("helper thread received sigusr %d\n", sig);
	return;
}

// dummy function to catch sigpipe when client disconnects
static void _sigpipe_handler(__attribute__((unused)) int signum)
{
	if(en_debug) printf("received sigpipe\n");
	return;
}


// reset a channel to all 0's as if the program just started
// preserve a few user-defined settings
static void _wipe_channel(int ch)
{
	if(ch<0 || ch>=N_CH) return;

	c[ch].running = 0;
	memset(c[ch].base_dir, 0, DIR_LEN);
	memset(c[ch].request_path, 0, DIR_LEN+16);
	memset(c[ch].control_path, 0, DIR_LEN+16);
	memset(c[ch].info_path, 0, DIR_LEN+16);
	c[ch].request_fd = 0;
	c[ch].control_fd = 0;
	c[ch].request_thread = 0;
	c[ch].control_thread = 0;
	c[ch].n_clients = 0;
	memset(c[ch].data_fd, 0, sizeof(int)*N_CLIENT);
	memset(c[ch].data_path, 0, N_CLIENT*PATH_LEN);
	memset(c[ch].client_names, 0, N_CLIENT*NAME_LEN);
	memset(c[ch].client_state, 0, sizeof(int)*N_CLIENT);
	memset(&c[ch].info, 0, sizeof(pipe_info_t));

	if(c[ch].info_json!=NULL){

		cJSON_Delete(c[ch].info_json);
		c[ch].info_json = NULL;
	}

	// everything else in the struct can be left alone

	return;
}


static void* _request_listener_func(void* context)
{
	char buf[256];
	int ch = (long)context;
	int bytes_read;

	// catch the SIGUSR1 signal which we use to quit the blocking read
	struct sigaction sigusr_action = {.sa_handler=_sigusr_cb};
	sigaction(SIGUSR1, &sigusr_action, 0);

	while(c[ch].running){
		bytes_read = read(c[ch].request_fd, buf, sizeof(buf));
		if(bytes_read>0){
			// successful read, make a new data pipe
			int client_id = pipe_server_add_client(ch, buf);
			// also inform the server via callback if it's been set
			if(client_id>=0 && c[ch].request_cb_func) {
				c[ch].request_cb_func(ch, buf, bytes_read, client_id, c[ch].request_cb_context);
			}
		}
		else if(bytes_read==0){
			// return of 0 means nobody is on the other end of the pipe
			// just sleep waiting for a client to start
			usleep(200000);
		}
		else{
			// actual error occured
			if(errno==EINTR) break; // caught the interrupt signal (ctrl-c), exit loop
			perror("request listener read error:");
			usleep(500000); // must sleep or we go into loop when other end of the pipe is closed
		}
	}

	if(en_debug) printf("channel %d request thread closing\n", ch);
	return NULL;
}


static void* _control_listener_func(void* context)
{
	int ch = (long)context;
	int bytes_read;

	// should never get here, creating the pipe should have set the read_buf_size
	// but check anyway since we are about to allocate memory
	if(c[ch].control_read_buf_size<=0){
		fprintf(stderr, "ERROR in control listener thread control read buf size must be nonzero\n");
		return NULL;
	}

	int buflen = c[ch].control_read_buf_size;
	char buf[buflen];

	// catch the SIGUSR1 signal which we use to quit the blocking read
	struct sigaction sigusr_action = {.sa_handler=_sigusr_cb};
	sigaction(SIGUSR1, &sigusr_action, 0);


	while(c[ch].running){

		if(c[ch].control_fd == 0){
			if(en_debug){
				fprintf(stderr, "channel %d helper tried to read from closed fd\n", ch);
			}
			break;
		}

		bytes_read = read(c[ch].control_fd, buf, buflen);

		// might have closed pipe while reading, if so exit thread
		if(!c[ch].running){
			break;
		}

		if(bytes_read<=0){
			if(en_debug){
				printf("read returned %d, errno: %d, server likely disconnected\n", bytes_read, errno);
				perror("errno=");
			}
			if(errno==EINTR) break; // caught the interrupt signal (ctrl-c), exit loop
		}
		else{
			// if we got a valid read and callback exists, execute it
			if(c[ch].control_cb_func){
				c[ch].control_cb_func(ch, buf, bytes_read, c[ch].control_cb_context);
			}
		}
	}

	if(en_debug) printf("channel %d control thread closing\n", ch);
	return NULL;
}



static cJSON* _make_new_cjson_from_info(pipe_info_t info)
{
	cJSON* new_json;

	new_json = cJSON_CreateObject();
	if(new_json == NULL){
		fprintf(stderr, "ERROR: in %s, failed to make new cJSON object\n", __FUNCTION__);
		return NULL;
	}

	if(cJSON_AddStringToObject(new_json, "name", info.name)==NULL){
		fprintf(stderr, "ERROR: could not add name to JSON object\n");
		return NULL;
	}
	if(cJSON_AddStringToObject(new_json, "location", info.location)==NULL){
		fprintf(stderr, "ERROR: could not add location to JSON object\n");
		return NULL;
	}
	if(cJSON_AddStringToObject(new_json, "type", info.type)==NULL){
		fprintf(stderr, "ERROR: could not add type to JSON object\n");
		return NULL;
	}
	if(cJSON_AddStringToObject(new_json, "server_name", info.server_name)==NULL){
		fprintf(stderr, "ERROR: could not add server_name to JSON object\n");
		return NULL;
	}
	if(cJSON_AddNumberToObject(new_json, "size_bytes", info.size_bytes)==NULL){
		fprintf(stderr, "ERROR: could not add size_bytes to JSON object\n");
		return NULL;
	}
	if(cJSON_AddNumberToObject(new_json, "server_pid", info.server_pid)==NULL){
		fprintf(stderr, "ERROR: could not add server_pid to JSON object\n");
		return NULL;
	}

	return new_json;
}


int pipe_server_create(int ch, pipe_info_t info, int flags)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(c[ch].running){
		fprintf(stderr, "ERROR in %s, channel %d already running\n", __FUNCTION__, ch);
		return -1;
	}

	// validate name
	if(strlen(info.name)<=0){
		fprintf(stderr, "ERROR in %s, invalid pipe name: %s\n", __FUNCTION__, info.name);
		return -1;
	}
	if(strstr(info.name, "/") != NULL){
		fprintf(stderr, "ERROR in %s, pipe name can't contain a '/'\n", __FUNCTION__);
		return -1;
	}
	if(strstr(info.name, "unknown") != NULL){
		fprintf(stderr, "ERROR in %s, pipe name can't be 'unknown'\n", __FUNCTION__);
		return -1;
	}

	// clean up the pipe path location in case the user messed it up
	char dir[MODAL_PIPE_MAX_DIR_LEN];
	if(info.location[0]!='/'){
		// user either gave a bad location or no location at all, construct from name
		if(pipe_expand_location_string(info.name, dir)){
			fprintf(stderr, "ERROR in %s, invalid pipe name: %s\n", __FUNCTION__, info.name);
		}
	}
	else{
		// user gave both name and location, check location for validity
		if(pipe_expand_location_string(info.location, dir)){
			fprintf(stderr, "ERROR in %s, invalid pipe location: %s\n", __FUNCTION__, info.location);
		}
	}

	// save the cleaned up location back to info struct
	strcpy(info.location, dir);

	// set the PID
	info.server_pid = (int)getpid();

	// validate pipe size
	if(info.size_bytes < (4*1024)){
		fprintf(stderr, "WARNING in %s, requested pipe size less than 4k, using default of 1M\n", __FUNCTION__);
		info.size_bytes = 1024*1024;
	}
	if(info.size_bytes>(256*1024*1024)){
		fprintf(stderr, "WARNING in %s, trying to set default pipe size >256MiB probably won't work\n", __FUNCTION__);
	}

	// fix control pipe size to defaults if the user didn't set them
	if(c[ch].control_pipe_size<=0){
		c[ch].control_pipe_size = 64*1024;
	}
	if(c[ch].control_read_buf_size<=0){
		c[ch].control_read_buf_size = 1024;
	}

	// lock mutex before we start accessing the state
	pthread_mutex_lock(&mtx[ch]);

	// check if directory is already being used by another channel
	for(int i=0; i<N_CH; i++){
		if(strcmp(dir, c[i].base_dir)==0){
			fprintf(stderr,"ERROR in %s, %s already in use by channel %d\n", __FUNCTION__, dir, i);
			pthread_mutex_unlock(&mtx[ch]);
			return -1;
		}
	}

	// turn on debug prints if requested
	if(flags & SERVER_FLAG_EN_DEBUG_PRINTS) en_debug = 1;

	// set up handler for sigpipe which occurs when client disconnects
	struct sigaction action;
	action.sa_handler = _sigpipe_handler;
	sigemptyset (&action.sa_mask);
	action.sa_flags = 0;
	sigaction (SIGPIPE, &action, NULL);

	// duplicate the directory into local mem to save for later
	// also make request and control paths
	strcpy(c[ch].base_dir, dir);
	strcpy(c[ch].request_path, dir);
	strcat(c[ch].request_path,"request");

	// make the directory for pipes to exist in
	if(_mkdir_recursive(c[ch].base_dir)){
		fprintf(stderr, "Error in %s making directory\n", __FUNCTION__);
		_wipe_channel(ch);
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}

	// construct info JSON and write to file
	c[ch].info_json = _make_new_cjson_from_info(info);
	if(c[ch].info_json == NULL){
		fprintf(stderr,"ERROR in %s, failed to construct json\n", __FUNCTION__);
		_wipe_channel(ch);
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}
	strcpy(c[ch].info_path, dir);
	strcat(c[ch].info_path,"info");
	if(json_write_to_file(c[ch].info_path, c[ch].info_json)){
		fprintf(stderr,"ERROR in %s, failed to write info json file\n", __FUNCTION__);
		_wipe_channel(ch);
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}

	// make the request pipe
	if(mkfifo(c[ch].request_path, 0666)){
		if(errno!=EEXIST){
			perror("Error in pipe_server_create calling mkfifo");
			_wipe_channel(ch);
			pthread_mutex_unlock(&mtx[ch]);
			return -1;
		}
	}
	// opening read only will block until something else opens the other end
	// so open read-write to avoid this even though we don't write to it
	c[ch].request_fd = open(c[ch].request_path, O_RDWR);
	if(c[ch].request_fd<0){
		perror("Error in pipe_server_create opening request path");
		_wipe_channel(ch);
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}

	// make the control pipe if enabled
	if(flags & SERVER_FLAG_EN_CONTROL_PIPE){
		// construct the string
		strcpy(c[ch].control_path,dir);
		strcat(c[ch].control_path,"control");
		// make pipe
		if(mkfifo(c[ch].control_path, 0666)){
			// if it already exists then don't worry
			if(errno!=EEXIST){
				perror("Error in pipe_server_create calling mkfifo");
				_wipe_channel(ch);
				pthread_mutex_unlock(&mtx[ch]);
				return -1;
			}
		}
		// opening read only will block until something else opens the other end
		// so open read-write to avoid this even though we don't write to it
		c[ch].control_fd = open(c[ch].control_path, O_RDWR);
		if(c[ch].control_fd<0){
			perror("Error in pipe_server_create opening control path");
			_wipe_channel(ch);
			pthread_mutex_unlock(&mtx[ch]);
			return -1;
		}

		// set the control pipe size
		errno = 0;
		int new_size = fcntl(c[ch].control_fd, F_SETPIPE_SZ, c[ch].control_pipe_size);

		// error check setting control pipe size
		if(new_size<c[ch].control_pipe_size){
			perror("ERROR failed to set control pipe size\n");
			if(errno == EPERM){
				fprintf(stderr, "You may need to be root to make a pipe that big\n");
			}
			_wipe_channel(ch);
			pthread_mutex_unlock(&mtx[ch]);
			return -1;
		}
	}

	// make the info pipe if enabled
	if(flags & SERVER_FLAG_EN_INFO_PIPE){
		// construct the string
		strcpy(c[ch].info_path,dir);
		strcat(c[ch].info_path,"info");
		// make pipe
		if(mkfifo(c[ch].info_path, 0666)){
			// if it already exists then don't worry
			if(errno!=EEXIST){
				perror("Error in pipe_server_create calling mkfifo");
				_wipe_channel(ch);
				pthread_mutex_unlock(&mtx[ch]);
				return -1;
			}
		}
	}

	// flag everything as initialized and start the request thread
	c[ch].info = info;
	c[ch].running = 1;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_create(&c[ch].request_thread, &attr, _request_listener_func, (void*)(long)ch);

	// optionally start the control thread
	if(flags & SERVER_FLAG_EN_CONTROL_PIPE){
		pthread_create(&c[ch].control_thread, &attr, _control_listener_func, (void*)(long)ch);
	}

	pthread_attr_destroy(&attr);

	// finally unlock the mutex
	pthread_mutex_unlock(&mtx[ch]);

	return 0;
}

int pipe_server_get_next_available_channel()
{
	for(int ch = 0; ch < N_CH; ch++){

		if(!c[ch].running){
			return ch;
		}
	}

	return PIPE_ERROR_OTHER;
}

cJSON* pipe_server_get_info_json_ptr(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH);
		return NULL;
	}
	if(!c[ch].running){
		fprintf(stderr, "ERROR in %s, channel %d not initialized yet\n", __FUNCTION__, ch);
		return NULL;
	}
	return c[ch].info_json;
}


int pipe_server_update_info(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH);
		return -1;
	}
	if(!c[ch].running){
		fprintf(stderr, "ERROR in %s, channel %d not initialized yet\n", __FUNCTION__, ch);
		return -1;
	}
	if(json_write_to_file(c[ch].info_path, c[ch].info_json)){
		fprintf(stderr,"ERROR in %s, failed to write info json file\n", __FUNCTION__);
		return -1;
	}
	return 0;
}


int pipe_server_add_client(int ch, const char* name)
{
	int new_client_id = -1;

	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH);
		return -1;
	}
	if(!c[ch].running){
		fprintf(stderr, "ERROR in %s, channel %d not initialized yet\n", __FUNCTION__, ch);
		return -1;
	}
	if(name==NULL){
		fprintf(stderr, "ERROR in %s, recevied NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// check the name length is within bounds
	int namelen = strlen(name);
	if(namelen >= NAME_LEN){
		fprintf(stderr, "ERROR in %s, name length is too long\n", __FUNCTION__);
		return -1;
	}

	// copy the string in and cleanup potential garbage at the end of the string
	char newname[NAME_LEN+1];
	strcpy(newname,name);
	for(int i=0;i<=namelen;i++){
		if(newname[i]<32 || newname[i]>122){
			newname[i]=0;
			break;
		}
	}

	// starting to manipulate the channel struct now, lock the mutex first
	pthread_mutex_lock(&mtx[ch]);

	// check if client already exists before making a new one
	for(int i=0; i<c[ch].n_clients; i++){
		if(strcmp(newname, c[ch].client_names[i])==0){
			if(en_debug) printf("client %s (id %d) reconnecting to channel %d\n", name, i, ch);
			new_client_id = i;
		}
	}

	// construct the string.
	char full_path[PATH_LEN];
	int len = sprintf(full_path, "%s%s", c[ch].base_dir, newname);
	if(len<0){
		perror("ERROR in pipe_server_add_client constructing path:");
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}
	len+=1; // add one to length to include the NULL character.


	// make the new pipe in the file system
	if(en_debug) printf("making new fifo %s\n", full_path);
	if(mkfifo(full_path, 0666)){
		if(errno!=EEXIST){
			perror("ERROR in pipe_server_add_client calling mkfifo:");
			pthread_mutex_unlock(&mtx[ch]);
			return -1;
		}
	}

	// open the pipe for nonblocking writes so if one client misbehaves and the
	// pipe overflows then the whole server doesn't hang up.
	// This may take a moment for the pipe to appear in the file system, so try
	// a few times until it succeeds.
	int fd, i;
	for(i=0;i<10;i++){
		fd = open(full_path, O_WRONLY | O_NONBLOCK);
		if(fd>0) break;
		usleep(50000);
	}

	// check if we failed to open the pipe
	if(fd<0){
		perror("ERROR in pipe_server_add_client calling open:");
		fprintf(stderr, "removing failed fifo %s\n", full_path);
		remove(full_path);
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}
	if(en_debug) printf("opened new pipe for writing after %d attempt(s)\n", i+1);

	// if this is a new client (not reconnecting) set up new id and name
	if(new_client_id<0){
		// new client gets the next id number
		new_client_id = c[ch].n_clients;
		// save the path string to check against later
		strcpy(c[ch].data_path[new_client_id], full_path);
		// save client name and increment n_client counter
		strcpy(c[ch].client_names[new_client_id], newname);
		c[ch].n_clients++;
	}

	// save the file descriptor and bump the counter
	c[ch].data_fd[new_client_id] = fd;
	c[ch].client_state[new_client_id] = CLIENT_INITIALIZED;

	// done manipulating the struct, unlock the mutex  before setting pipe size
	pthread_mutex_unlock(&mtx[ch]);

	// set default pipe size
	pipe_server_set_pipe_size(ch, new_client_id, c[ch].info.size_bytes);


	if(c[ch].connect_cb_func != NULL){
		c[ch].connect_cb_func(ch,new_client_id,c[ch].client_names[new_client_id],c[ch].connect_cb_context);
	}

	return new_client_id;
}

int pipe_server_bytes_in_pipe(int ch, int client_id)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(client_id<0 || client_id>=N_CLIENT){
		fprintf(stderr, "ERROR in %s, client_id should be between 0 & %d\n", __FUNCTION__, N_CLIENT-1);
		return -1;
	}
	if(!c[ch].data_fd[client_id]){
		fprintf(stderr, "ERROR in %s, channel %d client %d not initialized yet\n", __FUNCTION__, ch, client_id);
		return -1;
	}

	// lock mutex first
	pthread_mutex_lock(&mtx[ch]);

	// use ioctl to check
	int n_bytes;
	if(ioctl(c[ch].data_fd[client_id], FIONREAD, &n_bytes)){
		perror("ERROR in pipe_client_bytes_in_pipe");
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}

	// done, unlock mutex
	pthread_mutex_unlock(&mtx[ch]);
	return n_bytes;
}


int pipe_server_get_pipe_size(int ch, int client_id)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(client_id<0 || client_id>=N_CLIENT){
		fprintf(stderr, "ERROR in %s, client_id should be between 0 & %d\n", __FUNCTION__, N_CLIENT-1);
		return -1;
	}
	if(!c[ch].data_fd[client_id]){
		fprintf(stderr, "ERROR in %s, channel %d client %d not initialized yet\n", __FUNCTION__, ch, client_id);
		return -1;
	}

	pthread_mutex_lock(&mtx[ch]);
	int ret = fcntl(c[ch].data_fd[client_id], F_GETPIPE_SZ);
	pthread_mutex_unlock(&mtx[ch]);
	return ret;
}


int pipe_server_set_pipe_size(int ch, int client_id, int size_bytes)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(client_id<0 || client_id>=N_CLIENT){
		fprintf(stderr, "ERROR in %s, client_id should be between 0 & %d\n", __FUNCTION__, N_CLIENT-1);
		return -1;
	}
	if(!c[ch].data_fd[client_id]){
		fprintf(stderr, "ERROR in %s, channel %d client %d not initialized yet\n", __FUNCTION__, ch, client_id);
		return -1;
	}

	// use fctl with mutex protection
	pthread_mutex_lock(&mtx[ch]);
	errno = 0;
	int new_size = fcntl(c[ch].data_fd[client_id], F_SETPIPE_SZ, size_bytes);
	pthread_mutex_unlock(&mtx[ch]);

	// error check
	if(new_size<size_bytes){
#ifdef __ANDROID__
		LOGE( "ERROR failed to set pipe size");
#else				
		perror("ERROR failed to set pipe size");
#endif		
		if(errno == EPERM){
			fprintf(stderr, "You may need to be root to make a pipe that big\n");
		}
		// if fcntl fails, it may return 0 instead of the actual size, so fetch
		// the new size and return that instead.
		new_size = pipe_server_get_pipe_size(ch, client_id);
	}

	#ifdef __ANDROID__
		LOGI( "new pipe size %d", new_size);
	#endif

	// if fcntl was successful it returned the new size in bytes, so return it
	return new_size;
}

int pipe_server_set_control_pipe_size(int ch, int pipe_size, int read_buf_size)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(c[ch].running){
		fprintf(stderr, "ERROR in %s, must set control pipe size before creating the pipe\n", __FUNCTION__);
		return -1;
	}
	if(pipe_size<0){
		fprintf(stderr, "ERROR in %s, pipe_size must be >=0\n", __FUNCTION__);
		return -1;
	}
	if(read_buf_size<0){
		fprintf(stderr, "ERROR in %s, read_buf_size must be >=0\n", __FUNCTION__);
		return -1;
	}
	if(pipe_size>(256*1024*1024)){
		fprintf(stderr, "WARNING in %s, trying to set default pipe size >256MiB probably won't work\n", __FUNCTION__);
	}
	c[ch].control_pipe_size = pipe_size;
	c[ch].control_read_buf_size = read_buf_size;
	return 0;
}

int pipe_server_set_control_cb(int ch, server_control_cb* cb, void* context)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].control_cb_context = context;
	c[ch].control_cb_func = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}

int pipe_server_set_connect_cb(int ch, server_connect_cb* cb, void* context)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].connect_cb_context = context;
	c[ch].connect_cb_func = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}

int pipe_server_set_available_control_commands(int ch, const char* commands)
{
	//Server has not been initialized
	if(c[ch].info_json == NULL){
		return -1;
	}

	char buffer[2048];
	strcpy(buffer, commands);

	if(cJSON_HasObjectItem(c[ch].info_json, "available_commands")){
		cJSON_DeleteItemFromObject(c[ch].info_json, "available_commands");
	}

	cJSON* cmds = cJSON_CreateArray();

	cJSON_AddItemToObject(c[ch].info_json, "available_commands", cmds);

	// Extract the first token
	char * token = strtok(buffer, ",");
	// loop through the string to extract all other tokens
	while( token != NULL ) {
		cJSON_AddItemToArray(cmds, cJSON_CreateString(token));
		token = strtok(NULL, ",");
	}

	pipe_server_update_info(ch);

	return 0;

}


int pipe_server_set_disconnect_cb(int ch, server_disconnect_cb* cb, void* context)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].disconnect_cb_context = context;
	c[ch].disconnect_cb_func = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_server_write(int ch, const void* data, int bytes)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(bytes<=0){
		fprintf(stderr, "ERROR in %s, bytes to send must be >=0\n", __FUNCTION__);
		return -1;
	}
	// go through all clients, some may be disconnected, and that's okay.
	for(int i=0; i<c[ch].n_clients; i++){
		pipe_server_write_to_client(ch, i, data, bytes);
	}
	return 0;
}


int pipe_server_write_to_client(int ch, int client_id, const void* data, int bytes)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(client_id<0 || client_id>=N_CLIENT){
		fprintf(stderr, "ERROR in %s, client_id should be between 0 & %d\n", __FUNCTION__, N_CLIENT-1);
		return -1;
	}
	if(!c[ch].data_fd[client_id]) return -1;

	// client has disconnected, nothing to write.
	if(c[ch].client_state[client_id] == CLIENT_DISCONNECTED){
		return -1;
	}

	// try a write
	pthread_mutex_lock(&mtx[ch]);
	int result = write(c[ch].data_fd[client_id], data, bytes);

	// optional debug prints
	if(en_debug){
		fprintf(stderr, "write to ch: %d id: %d result: %d errno: %d\n", ch, client_id, result, errno);
		if(result!=bytes) perror("write error");
		fprintf(stderr, "previous client state was %d\n", c[ch].client_state[client_id]);
	}

	// decide the state of the client from the write value
	if(result==bytes){
		// write was successfull! Flag the client as connected and return :)
		c[ch].client_state[client_id] = CLIENT_CONNECTED;
		pthread_mutex_unlock(&mtx[ch]);
		return 0;
	}

	// partial write, possible if the pipe is full
	if(result>0){
		fprintf(stderr, "ERROR in %s, tried to write %d bytes but write returned %d\n", __FUNCTION__, bytes, result);
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}

	// if we got here, write threw a real error, most likely client disconnected
	// TODO it's possible the CLIENT_INITIALIZED check should be replaced by a time
	// since initialization since the client may just still be opening the pipe for read
	int last_state = c[ch].client_state[client_id];
	if(last_state == CLIENT_CONNECTED || last_state == CLIENT_INITIALIZED){
		// you can print this in the disconnect callback if you want to keep
		// this message but don't want to enable verbose debug mode.
		// see modal-hello-server for an example
		if(en_debug){
			fprintf(stderr,"Client %s (id %d) disconnected from channel %d\n", \
									c[ch].client_names[client_id], client_id, ch);
		}
		// flag as disconnected
		c[ch].client_state[client_id] = CLIENT_DISCONNECTED;
		// delete the pipe indicating to other clients they can request this name
		close(c[ch].data_fd[client_id]);
		c[ch].data_fd[client_id] = 0;
		remove(c[ch].data_path[client_id]);
		// call disconnect cb if user has set one
		if(c[ch].disconnect_cb_func!=NULL){
			c[ch].disconnect_cb_func(ch, client_id, c[ch].client_names[client_id], c[ch].disconnect_cb_context);
		}
		// now the client is flagged as disconnected but we keep their name in
		// memory as a previously registered client, then we know if they reconnect
	}

	pthread_mutex_unlock(&mtx[ch]);
	return -1;
}


int pipe_server_write_camera_frame(int ch, camera_image_metadata_t meta, const void* data)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR in %s, received NULL data pointer\n", __FUNCTION__);
		return -1;
	}
	if(meta.size_bytes<=0){
		fprintf(stderr, "ERROR in %s, metadata must specify a positive frame size in bytes\n", __FUNCTION__);
		return -1;
	}

	// set the magic number so the user doesn't have to
	meta.magic_number = CAMERA_MAGIC_NUMBER;

	// go through all clients, some may be disconnected, and that's okay.
	for(int i=0; i<c[ch].n_clients; i++){
		// first try writing metadata
		int ret = pipe_server_write_to_client(ch, i, (char*)&meta, sizeof(camera_image_metadata_t));
#ifdef __ANDROID__		
LOGI( "wrote meta %d %d", ret, meta.size_bytes );		
#endif
		// only write the camera frame if the metadata succeeded
		if(ret==0){
			pipe_server_write_to_client(ch, i, data, meta.size_bytes);
#ifdef __ANDROID__			
LOGI( "wrote %d bytes %d", meta.size_bytes, ret );			
#endif
		}
	}
	return 0;
}


int pipe_server_write_stereo_frame(int ch, camera_image_metadata_t meta, const void* left, const void* right)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(left==NULL){
		fprintf(stderr, "ERROR in %s, received NULL data pointer\n", __FUNCTION__);
		return -1;
	}
	if(right==NULL){
		fprintf(stderr, "ERROR in %s, received NULL data pointer\n", __FUNCTION__);
		return -1;
	}
	if(meta.size_bytes<=0){
		fprintf(stderr, "ERROR in %s, metadata must specify a positive frame size in bytes\n", __FUNCTION__);
		return -1;
	}
	if(meta.size_bytes%2){
		fprintf(stderr, "ERROR in %s, metadata must specify an even number of bytes\n", __FUNCTION__);
		return -1;
	}

	// set the magic number so the user doesn't have to
	meta.magic_number = CAMERA_MAGIC_NUMBER;

	// go through all clients, some may be disconnected, and that's okay.
	for(int i=0; i<c[ch].n_clients; i++){
		// first try writing metadata
		int ret = pipe_server_write_to_client(ch, i, (char*)&meta, sizeof(camera_image_metadata_t));
		// only write the camera frame if the metadata succeeded
		if(ret==0){
			ret = pipe_server_write_to_client(ch, i, left, meta.size_bytes/2);
		}
		// only write the right camera frame if the left succeeded
		if(ret==0){
			ret = pipe_server_write_to_client(ch, i, right, meta.size_bytes/2);
		}
	}
	return 0;
}


int pipe_server_write_point_cloud(int ch, point_cloud_metadata_t meta, const void* data)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR in %s, received NULL data pointer\n", __FUNCTION__);
		return -1;
	}

	// set the magic number so the user doesn't have to
	meta.magic_number = POINT_CLOUD_MAGIC_NUMBER;

	int size_bytes = pipe_point_cloud_meta_to_size_bytes(meta);
	if(size_bytes<0){
		fprintf(stderr, "ERROR in %s, bad metadata\n", __FUNCTION__);
		return -1;
	}

	// go through all clients, some may be disconnected, and that's okay.
	for(int i=0; i<c[ch].n_clients; i++){
		// first try writing metadata
		int ret = pipe_server_write_to_client(ch, i, (char*)&meta, sizeof(point_cloud_metadata_t));
		// only write the points if the metadata succeeded and more than 1 point is present
		if(ret==0 && meta.n_points>0){
			pipe_server_write_to_client(ch, i, (char*)data, size_bytes);
		}
	}
	return 0;
}



int pipe_server_get_client_state(int ch, int client_id)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(client_id<0 || client_id>=N_CLIENT){
		fprintf(stderr, "ERROR in %s, client_id should be between 0 & %d\n", __FUNCTION__, N_CLIENT-1);
		return -1;
	}

	return c[ch].client_state[client_id];
}


int pipe_server_get_num_clients(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	pthread_mutex_lock(&mtx[ch]);
	int n_connected = 0;

	// count the number of clients that are either connected or initialized.
	// This is because a client is not marked as connected until AFTER the first
	// successful transfer. Between the client sending the request and reading
	// the first packet they are instead "initialized".
	for(int i=0;i<c[ch].n_clients;i++){
		if( c[ch].client_state[i]==CLIENT_CONNECTED ||
			c[ch].client_state[i]==CLIENT_INITIALIZED ){
			n_connected++;
		}
	}
	pthread_mutex_unlock(&mtx[ch]);
	return n_connected;
}


int pipe_server_get_client_id_from_name(int ch, char* name)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(name==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	int ret = -1;
	pthread_mutex_lock(&mtx[ch]);

	// search through clients for a match
	for(int i=0; i<c[ch].n_clients; i++){
		if(strcmp(name, c[ch].client_names[i])==0){
			ret = i;
			break;
		}
	}
	pthread_mutex_unlock(&mtx[ch]);
	return ret;
}


char* pipe_server_get_client_name_from_id(int ch, int client_id)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return NULL;
	}
	if(client_id<0 || client_id>=N_CLIENT){
		fprintf(stderr, "ERROR in %s, client id must be >=0\n", __FUNCTION__);
		return NULL;
	}
	// This will automatically be NULL if client hasn't initialized yet
	return c[ch].client_names[client_id];
}


void pipe_server_close(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return;
	}

	// nothing to do if not running
	if(!c[ch].running) return;

	// this is the main use for the mutex, locking the channel during init/close
	pthread_mutex_lock(&mtx[ch]);

	// signal the request and control threads to stop and disable callbacks
	c[ch].running = 0;
	c[ch].n_clients = 0;
	c[ch].control_cb_func = NULL;
	c[ch].request_cb_func = NULL;
	c[ch].disconnect_cb_func = NULL;

	// send signal to request thread, this is just to make the blocking read quit
	pthread_kill(c[ch].request_thread, SIGUSR1);

	// android doesn't have pthread_timedjoin_np
#ifdef __ANDROID__
	errno = pthread_join(c[ch].request_thread, NULL);
#else
	// do a timed join, 1 second timeout
	struct timespec thread_timeout;
	clock_gettime(CLOCK_REALTIME, &thread_timeout);
	thread_timeout.tv_sec += 1;
	errno = pthread_timedjoin_np(c[ch].request_thread, NULL, &thread_timeout);
#endif

	if(errno==ETIMEDOUT){
		fprintf(stderr, "WARNING, %s timed out joining request thread\n", __FUNCTION__);
	}

	// cleanup request thread stuff
	close(c[ch].request_fd);
	remove(c[ch].request_path);

	// join control thread too if it was enabled
	if(c[ch].control_thread){
		// send signal to control thread, this is just to make the blocking read quit
		pthread_kill(c[ch].control_thread, SIGUSR1);

	// android doesn't have pthread_timedjoin_np
#ifdef __ANDROID__
		errno = pthread_join(c[ch].control_thread, NULL);
#else
		// do a timed join, 1 second timeout
		clock_gettime(CLOCK_REALTIME, &thread_timeout);
		thread_timeout.tv_sec += 1;
		errno = pthread_timedjoin_np(c[ch].control_thread, NULL, &thread_timeout);
#endif

		if(errno==ETIMEDOUT){
			fprintf(stderr, "WARNING, %s timed out joining request thread\n", __FUNCTION__);
		}

		// cleanup control pipe stuff
		close(c[ch].control_fd);
		remove(c[ch].control_path);
	}

	// close the data pipes
	for(int i=0; i<c[ch].n_clients; i++){
		close(c[ch].data_fd[i]);
	}

	// delete the pipe directory form the file system
	_remove_recursive(c[ch].base_dir);

	// all done, wipe the channel's data struct back to 0 and release the mutex
	_wipe_channel(ch);
	pthread_mutex_unlock(&mtx[ch]);
	return;
}



void pipe_server_close_all(void)
{
	for(int i=0; i<N_CH; i++) pipe_server_close(i);
	return;
}


////////////////////////////////////////////////////////////////////////////////
// DEPRECATED FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

// DEPRECATED
void pipe_server_close_all_channels(void)
{
	pipe_server_close_all();
	return;
}

// DEPRECATED
void pipe_server_close_channel(int ch)
{
	pipe_server_close(ch);
	return;
}

// DEPRECATED
int pipe_server_init_channel(int ch, const char* topic, int flags)
{
	pipe_info_t info = PIPE_INFO_INITIALIZER;

	// clean up the pipe path location in case the user messed it up
	char dir[MODAL_PIPE_MAX_DIR_LEN];
	if(pipe_expand_location_string((char*)topic, dir)<0){
		fprintf(stderr, "ERROR in %s, invalid pipe location: %s\n", __FUNCTION__, info.location);
		return -1;
	}
	strcpy(info.location, dir);

	// find the start of the pipe name
	int dirlen = strlen(dir);
	int start = 0;
	for(int i=dirlen-3;i>=0;i--){
		if(dir[i]=='/'){
			start = i+1;
			break;
		}
	}
	// copy pipe name out of the full path
	if(start>0){
		memcpy(info.name, &dir[start], dirlen-start-1);
		info.name[dirlen-start-1] = 0;
	}

	return pipe_server_create(ch, info, flags);
}


// DEPRECATED
int pipe_server_send_to_channel(int ch, char* data, int bytes){
	return pipe_server_write(ch, data, bytes);
}

// DEPRECATED
int pipe_server_send_camera_frame_to_channel(int ch, camera_image_metadata_t meta, char* data)
{
	return pipe_server_write_camera_frame(ch, meta, data);
}

// DEPRECATED
int pipe_server_send_stereo_frame_to_channel(int ch, camera_image_metadata_t meta, char* left, char* right)
{
	return pipe_server_write_stereo_frame(ch, meta, left, right);
}

// DEPRECATED
int pipe_server_send_point_cloud_to_channel(int ch, point_cloud_metadata_t meta, float* data)
{
	return pipe_server_write_point_cloud(ch, meta, data);
}

// DEPRECATED
int pipe_server_send_to_client(int ch, int client_id, char* data, int bytes)
{
	return pipe_server_write_to_client(ch, client_id, data, bytes);
}

// DEPRECATED FUNCTION
int pipe_server_set_request_cb(int ch, server_request_cb* cb, void* context)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].request_cb_context = context;
	c[ch].request_cb_func = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}

// DEPRECATED
int pipe_server_set_default_pipe_size(int ch, int size_bytes)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return -1;
	}
	if(size_bytes<0){
		fprintf(stderr, "ERROR in %s, size_bytes must be >=0\n", __FUNCTION__);
		return -1;
	}
	if(size_bytes>(256*1024*1024)){
		fprintf(stderr, "WARNING in %s, trying to set default pipe size >256MiB probably won't work\n", __FUNCTION__);
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].info.size_bytes = size_bytes;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}

// DEPRECATED
int pipe_server_set_info_string(__attribute__((unused))int ch, __attribute__((unused))const char* string)
{
	fprintf(stderr, "ERROR pipe_server_set_info_string() is now deprecated\n");
	fprintf(stderr, "Please use pipe_server_get_info_json_ptr() and\n");
	fprintf(stderr, "pipe_server_update_info_json() instead\n");
	return -1;

}