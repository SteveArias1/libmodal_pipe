/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional, most clients won't use it
// include mavlink BEFORE interfaces so we get the mavlink helper included
#include <c_library_v2/common/mavlink.h>
#endif // ENABLE_MAVLINK_SUPPORT
#include <modal_pipe_interfaces.h>
#include <modal_pipe_deprecated.h>


tag_detection_t* pipe_validate_tag_detection_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an vio_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	tag_detection_t* new_ptr = (tag_detection_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating tag detection received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating tag detection received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(tag_detection_t)){
		fprintf(stderr, "ERROR validating tag detection received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(tag_detection_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(tag_detection_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic_number != TAG_DETECTION_MAGIC_NUMBER) n_failed++;
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating tag detection received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}


const char* pipe_tag_location_type_to_string(int i)
{
	switch(i){
		case TAG_LOCATION_UNKNOWN:
			return "unknown";
		case TAG_LOCATION_FIXED:
			return "fixed";
		case TAG_LOCATION_STATIC:
			return "static";
		case TAG_LOCATION_DYNAMIC:
			return "dynamic";
		default:
			return "unknown";
	}
}

const char* pipe_image_format_to_string(int i)
{
	switch(i){
		case IMAGE_FORMAT_RAW8:
			return "RAW8";
		case IMAGE_FORMAT_NV12:
			return "NV12";
		case IMAGE_FORMAT_STEREO_RAW8:
			return "STEREO_RAW8";
		case IMAGE_FORMAT_H264:
			return "H264";
		case IMAGE_FORMAT_H265:
			return "H265";
		case IMAGE_FORMAT_RAW16:
			return "RAW16";
		case IMAGE_FORMAT_NV21:
			return "NV21";
		case IMAGE_FORMAT_JPG:
			return "JPG";
		case IMAGE_FORMAT_YUV422:
			return "YUV422";
		case IMAGE_FORMAT_YUV420:
			return "YUV420";
		case IMAGE_FORMAT_RGB:
			return "RGB";
		case IMAGE_FORMAT_FLOAT32:
			return "FLOAT32";
		default:
			return "Unknown Format";
	}
}


tof_data_t* pipe_validate_tof_data_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an tof_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	tof_data_t* new_ptr = (tof_data_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating TOF data received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating TOF data received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(tof_data_t)){
		fprintf(stderr, "ERROR validating TOF data received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(tof_data_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(tof_data_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic_number != TOF_MAGIC_NUMBER) n_failed++;
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating TOF data received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}


imu_data_t* pipe_validate_imu_data_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an imu_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	imu_data_t* new_ptr = (imu_data_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating IMU data received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating IMU data received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(imu_data_t)){
		fprintf(stderr, "ERROR validating IMU data received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(imu_data_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(imu_data_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic_number != IMU_MAGIC_NUMBER) n_failed++;
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating IMU data received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}

const char* pipe_point_cloud_format_to_string(int i)
{
	switch(i){
		case POINT_CLOUD_FORMAT_FLOAT_XYZ:
			return "Float XYZ";
	    case POINT_CLOUD_FORMAT_FLOAT_XYZC:
			return "Float XYZC";
	    case POINT_CLOUD_FORMAT_FLOAT_XYZRGB:
			return "Float XYZ, Int RGB";
	    case POINT_CLOUD_FORMAT_FLOAT_XYZCRGB:
			return "Float XYZC, Int RGB";
	    case POINT_CLOUD_FORMAT_FLOAT_XY:
			return "Float XY";
	    case POINT_CLOUD_FORMAT_FLOAT_XYC:
			return "Float XYC";
		default:
			return "Unknown Format";
	}
}

int pipe_point_cloud_meta_to_size_bytes(point_cloud_metadata_t meta)
{
	switch( meta.format ){
		case POINT_CLOUD_FORMAT_FLOAT_XYZ:
			return meta.n_points * 3 * sizeof(float);

	    case POINT_CLOUD_FORMAT_FLOAT_XYZC:
			return meta.n_points * 4 * sizeof(float);

	    case POINT_CLOUD_FORMAT_FLOAT_XYZRGB:
			return meta.n_points * ((3 * sizeof(float)) + 3);

	    case POINT_CLOUD_FORMAT_FLOAT_XYZCRGB:
			return meta.n_points * ((4 * sizeof(float)) + 3);

	    case POINT_CLOUD_FORMAT_FLOAT_XY:
			return meta.n_points * 2 * sizeof(float);

	    case POINT_CLOUD_FORMAT_FLOAT_XYC:
			return meta.n_points * 3 * sizeof(float);

	}

	fprintf(stderr, "ERROR in %s, invalid point cloud format: %d\n", __FUNCTION__, meta.format);
	return -1;
}


#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional, most clients won't use it
mavlink_message_t* pipe_validate_mavlink_message_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an vio_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	mavlink_message_t* new_ptr = (mavlink_message_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating mavlink_message_t data received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating mavlink_message_t data received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(mavlink_message_t)){
		fprintf(stderr, "ERROR validating mavlink_message_t data received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(pose_4dof_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(mavlink_message_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic != MAVLINK_STX){
			n_failed++;
		}
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating mavlink_message_t data received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}
#endif // ENABLE_MAVLINK_SUPPORT

pose_4dof_t* pipe_validate_pose_4dof_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an vio_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	pose_4dof_t* new_ptr = (pose_4dof_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating pose_4dof data received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating pose_4dof data received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(pose_4dof_t)){
		fprintf(stderr, "ERROR validating pose_4dof data received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(pose_4dof_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(pose_4dof_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic_number != POSE_4DOF_MAGIC_NUMBER) n_failed++;
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating pose_4dof data received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}


pose_vel_6dof_t* pipe_validate_pose_vel_6dof_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an vio_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	pose_vel_6dof_t* new_ptr = (pose_vel_6dof_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating pose_vel_6dof_t data received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating pose_vel_6dof_t data received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(pose_vel_6dof_t)){
		fprintf(stderr, "ERROR validating pose_vel_6dof_t data received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(pose_vel_6dof_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(pose_vel_6dof_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic_number != POSE_VEL_6DOF_MAGIC_NUMBER) n_failed++;
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating pose_vel_6dof_t data received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}


vio_data_t* pipe_validate_vio_data_t(char* data, int bytes, int* n_packets)
{
	// cast raw data from buffer to an vio_data_t array so we can read data
	// without memcpy. Also write out packets read as 0 until we validate data.
	vio_data_t* new_ptr = (vio_data_t*) data;
	*n_packets = 0;

	// basic sanity checks
	if(bytes<0){
		fprintf(stderr, "ERROR validating VIO data received through pipe: number of bytes = %d\n", bytes);
		return NULL;
	}
	if(data==NULL){
		fprintf(stderr, "ERROR validating VIO data received through pipe: got NULL data pointer\n");
		return NULL;
	}
	if(bytes%sizeof(vio_data_t)){
		fprintf(stderr, "ERROR validating VIO data received through pipe: read partial packet\n");
		fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes, sizeof(vio_data_t));
		return NULL;
	}

	// calculate number of packets locally until we validate each packet
	int n_packets_tmp = bytes/sizeof(vio_data_t);

	// check if any packets failed the magic number check
	int i, n_failed = 0;
	for(i=0;i<n_packets_tmp;i++){
		if(new_ptr[i].magic_number != VIO_MAGIC_NUMBER) n_failed++;
	}
	if(n_failed>0){
		fprintf(stderr, "ERROR validating VIO data received through pipe: %d of %d packets failed\n", n_failed, n_packets_tmp);
		return NULL;
	}

	// if we get here, all good. Write out the number of packets read and return
	// the new cast pointer. It's the same pointer the user provided but cast to
	// the right type for simplicity and easy of use.
	*n_packets = n_packets_tmp;
	return new_ptr;
}


void pipe_print_vio_state(int s)
{
	if     (s == VIO_STATE_FAILED)          printf("FAIL");
	else if(s == VIO_STATE_INITIALIZING)    printf("INIT");
	else if(s == VIO_STATE_OK)              printf("OKAY");
	else printf("UNKNOWN_VIO_STATE");
	return;
}


void pipe_print_vio_error(int e)
{
	if(e & ERROR_CODE_COVARIANCE)     printf("COV_ERROR ");
	if(e & ERROR_CODE_IMU_OOB)        printf("IMU_OOB ");
	if(e & ERROR_CODE_IMU_BW)         printf("IMU_BW ");
	if(e & ERROR_CODE_NOT_STATIONARY) printf("NOT_STATIONARY ");
	if(e & ERROR_CODE_NO_FEATURES)    printf("NO_FEATURES ");
	if(e & ERROR_CODE_CONSTRAINT)     printf("CONSTRAINT_ERROR ");
	if(e & ERROR_CODE_FEATURE_ADD)    printf("FEATURE_ADD_ERROR ");
	if(e & ERROR_CODE_VEL_INST_CERT)  printf("VEL_INST_CERT ");
	if(e & ERROR_CODE_VEL_WINDOW_CERT)printf("VEL_WINDOW_CERT ");
	if(e & ERROR_CODE_DROPPED_IMU)    printf("DROPPED_IMU ");
	if(e & ERROR_CODE_BAD_CAM_CAL)    printf("BAD_CAM_CAL ");
	if(e & ERROR_CODE_LOW_FEATURES)   printf("LOW_FEATURES ");
	if(e & ERROR_CODE_DROPPED_CAM)    printf("DROPPED_CAM ");
	if(e & ERROR_CODE_DROPPED_GPS_VEL)printf("DROPPED_GPS_VEL ");
	if(e & ERROR_CODE_BAD_TIMESTAMP)  printf("BAD_TIMESTAMP ");
	if(e & ERROR_CODE_UNKNOWN)        printf("UNKNOWN_ERROR ");
	if(e & ERROR_CODE_IMU_MISSING)    printf("IMU_MISSING ");
	if(e & ERROR_CODE_CAM_MISSING)    printf("CAM_MISSING ");
	if(e & ERROR_CODE_CAM_BAD_RES)    printf("CAM_BAD_RES ");
	if(e & ERROR_CODE_CAM_BAD_FORMAT) printf("CAM_BAD_FORMAT ");
	return;
}



// DEPRECATED
const char* modal_image_format_name(int i)
{
	return pipe_image_format_to_string(i);
}

// DEPRECATED
tof_data_t* modal_tof_validate_pipe_data(char* data, int bytes, int* n_packets)
{
	return pipe_validate_tof_data_t(data, bytes, n_packets);
}

// DEPRECATED
imu_data_t* modal_imu_validate_pipe_data(char* data, int bytes, int* n_packets)
{
	return pipe_validate_imu_data_t(data, bytes, n_packets);
}

// DEPRECATED
pose_4dof_t* modal_pose_4dof_validate_pipe_data(char* data, int bytes, int* n_packets)
{
	return pipe_validate_pose_4dof_t(data, bytes, n_packets);
}

// DEPRECATED
pose_vel_6dof_t* modal_pose_vel_6dof_validate_pipe_data(char* data, int bytes, int* n_packets)
{
	return pipe_validate_pose_vel_6dof_t(data, bytes, n_packets);
}

// DEPRECATED
vio_data_t* modal_vio_validate_pipe_data(char* data, int bytes, int* n_packets)
{
	return pipe_validate_vio_data_t(data, bytes, n_packets);
}

// DEPRECATED
void modal_vio_print_state(int s)
{
	return pipe_print_vio_state(s);
}

// DEPRECATED
void modal_vio_print_error_code(int e)
{
	return pipe_print_vio_error(e);
}
