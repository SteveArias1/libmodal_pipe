/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef MODAL_START_STOP_H
#define MODAL_START_STOP_H

#ifdef __cplusplus
extern "C" {
#endif

// This is set to 0 by the signal handler, you should set it to 1 when you are
// done initializing then so your main() while loop can check this for shutdown.
extern volatile int main_running;

/**
 * @brief      Makes a PID file PID_FILE (/run/name.pid) containing the current
 *             PID of your process
 *
 * @param[in]  name  The desired name, usually name of the executable
 *
 * @return     Returns 0 if successful. If that file already exists then it is
 *             not touched and this function returns 1 at which point we suggest
 *             you run kill_exising_process() to kill that process if you only
 *             want one instance running at a time. Returns -1 if there is some
 *             other problem writing to the file.
 */
int make_pid_file(const char* name);


/**
 * @brief      This function is used to make sure any existing program using the
 *             PID file (/run/name.pid) is stopped.
 *
 *             The user doesn't need to integrate this in their own program
 *             However, it's nice if you only want one instance of a process
 *             ever to run at the same time. For example, voxl-vision-px4 starts
 *             as a background process via systemd but users may want to run it
 *             in interactive mode for debugging. voxl-vision-px4 will use this
 *             function to kill the background process before starting itself
 *             again.
 *
 * @param[in]  name       process name to stop, usually your own
 * @param[in]  timeout_s  timeout period to wait for process to close cleanly,
 *                        must be >=0.1, 2.0 seconds is usually good.
 *
 * @return     return values:
 * - -4: invalid argument or other error
 * - -3: insufficient privileges to kill existing process
 * - -2: unreadable or invalid contents in PID_FILE
 * - -1: existing process failed to close cleanly and had to be killed
 * -  0: No existing process was running
 * -  1: An existing process was running but it shut down cleanly.
 */
int kill_existing_process(const char* name, float timeout_s);


/**
 * @brief      Removes the PID file (/run/name.pid) created by make_pid_file().
 *
 *             This should be called before your program closes to make sure its
 *             PID file isn't left in the file system.
 *
 * @param[in]  name  The name of the process to remove the associated file
 *                   (/run/name.pid)
 *
 * @return     Returns 0 whether or not the file was actually there. Returns -1
 *             if there was a file system error.
 */
int remove_pid_file(const char* name);


/**
 * @brief      Enables a generic signal handler. Optional but recommended.
 *
 *             This catches SIGINT, SIGTERM, SIGHUP, and SIGSEGV with the
 *             following behavior:
 *
 * - SIGINT (ctrl-c) and SIGTERM: Sets the global variable main_running to 0
 *     indicating to the process that it's time for threads to shut down cleanly
 * - SIGHUP: Ignored to prevent process from stopping due to bad network
 *     connection when starting processes over SSH
 * - SIGSEGV:  Segfaults will be caught and print some debugging info to stderr
 *     before setting main_running to 0. Behavior with segfaults is not
 *     guaranteed to be predictable.
 *
 * @return     Returns 0 on success or -1 on error
 */
int enable_signal_handler(void);


/**
 * @brief      Disables the signal handlers enabled by enable_signal_handler
 *
 * @return     { description_of_the_return_value }
 */
int disable_signal_handler(void);



#ifdef __cplusplus
}
#endif

#endif // MODAL_START_STOP_H

